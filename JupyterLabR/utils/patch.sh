#!/bin/bash
set -e

# Patching jupyterlab
cp -v /tmp/utils/config/jupyterlab-topbar-extension/plugin.jupyterlab-settings /opt/conda/share/jupyter/lab/settings/
cp -v /tmp/utils/config/jupyter_notebook_config.py /etc/jupyter/
cp -v /tmp/utils/logo.png /opt/conda/lib/python3.6/site-packages/notebook/static/favicon.ico
cp -v /tmp/utils/logo.png /opt/conda/lib/python3.6/site-packages/notebook/static/base/images/favicon.ico
cp -v /tmp/utils/logo.png /opt/conda/share/jupyterhub/static/favicon.ico
cp -v /tmp/utils/logo.png /opt/conda/share/jupyterhub/static/images/jupyter.png
cp -v /tmp/utils/alphienlab.svg /opt/conda/share/jupyter/lab/staging/node_modules/@jupyterlab/ui-components/style/icons/jupyter/jupyter.svg
cp -v /tmp/utils/alphienlab.svg /opt/conda/share/jupyter/lab/staging/node_modules/@jupyterlab/ui-components/style/icons/jupyter/jupyter-favicon.svg

sed 's#<title>JupyterLab</title>#<title>AlphienLab</title>#g' -i /opt/conda/lib/python3.6/site-packages/jupyterlab/static/index.html
sed 's#<title>JupyterLab</title>#<title>AlphienLab</title>#g' -i /opt/conda/share/jupyter/lab/static/index.html
sed 's#<title>JupyterLab</title>#<title>AlphienLab</title>#g' -i /opt/conda/share/jupyter/lab/staging/build/index.html

# Jupyterhub
sed 's#<title>{% block title %}JupyterHub{% endblock %}</title>#<title>{% block title %}AlphienLab{% endblock %}</title>#g' -i /opt/conda/share/jupyterhub/templates/page.html

# Alphienstudio
cp -v /tmp/utils/studio/favicon.ico /opt/conda/lib/python3.6/site-packages/jupyter_rsession_proxy/icons/
#COPY utils/studio/favicon.ico /opt/conda/lib/python3.6/site-packages/jupyter_rsession_proxy/icons/rstudio.svg

# Code formatter
#cp -v /tmp/utils/config/settings.json /opt/conda/share/jupyter/lab/schemas/@ryantam626/jupyterlab_code_formatter/settings.json

# Enable sourcing qlib
cp -v /tmp/utils/R/Renviron /etc/R/
cp -v /tmp/utils/R/Rprofile.site /etc/R/

# Install menu_stack
jupyter nbextension install /tmp/utils/R/menu_stack --system --log-level='ERROR' && \
jupyter nbextension enable menu_stack/main --system && \
chmod -R 755 /usr/local/share/jupyter/nbextensions/menu_stack/

# Add custom message when open terminal
echo 'echo -e "Welcome to Alphien Terminal\n"' >> /etc/profile

# Add overrides.json setting for custom css
cp -v /tmp/utils/config/overrides.json /opt/conda/share/jupyter/lab/settings/