# Copyright (c) Jupyter Development Team.
# Distributed under the terms of the Modified BSD License.

from jupyter_core.paths import jupyter_data_dir
import subprocess
import os
import errno
import stat

c = get_config()
c.NotebookApp.ip = '0.0.0.0'
c.NotebookApp.port = 8888
#c.NotebookApp.open_browser = False

# https://github.com/jupyter/notebook/issues/3130
c.FileContentsManager.delete_to_trash = False

# Change default umask for all subprocesses of the notebook server if set in
# the environment
if 'NB_UMASK' in os.environ:
    os.umask(int(os.environ['NB_UMASK'], 8))


# CORS handling
origin = '*'
c.NotebookApp.allow_origin = origin
c.NotebookApp.tornado_settings = {
    'headers': {
        'Content-Security-Policy': "frame-ancestors https://*.alphien.com",
        'Access-Control-Allow-Origin': origin,
        'Access-Control-Allow-Credentials': "false",
        'Access-Control-Allow-Methods': "PUT, GET, POST, DELETE, OPTIONS"
    }
}

# Change file without confirmation
c.TerminalInteractiveShell.confirm_exit = False
